<!DOCTYPE html>

<html lang="en" class="gr__v4-alpha_getbootstrap_com">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">

	<title>Dream In Color | Technology for Humanity | Higher Education</title>
	<link rel="canonical" href="">

	<!-- Bootstrap core CSS -->
	<link href="/dist/css/main.min.css" rel="stylesheet">
	<!-- Fullpage CSS -->
	<link href="/dist/css/fullpage.min.css" rel="stylesheet">


	<link href="/dist/css/slick.css" rel="stylesheet">
	<link href="/dist/css/slick-theme.css" rel="stylesheet">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ"
	    crossorigin="anonymous">

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,700,800" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
</head>

<body>
	<!-- Main Wrapper -->
	<div class="main-wrapper">
		<!-- Include for header -->
		<div id="header-include">
		</div>

		<!-- Include for menu -->
		<div id="menu-include">
		</div>

		<!-- Banner -->
		<div class="tech-redesigned-page-banner page-banner text-center">
			<div class="overlay_purple"></div>
			<div class="banner-content">
				<h1>TECHNOLOGY</h1>
				<h2>redesigned.</h2>
			</div>
			<!-- /.banner-content -->
		</div>
		<!-- /.page-banner -->

		<!-- Banner bottom content -->
		<div class="section-improve-your-technology">
			<div class="container text-center">
				<h2>IMPROVE YOUR TECHNOLOGY BY IMPROVING <br> THE USER EXPERIENCE!</h2>
				<p>The design phase is all about the "push and pull". The "push" is the layout, content, and visual design that you are presenting to your users which ultimately becomes the "pull" - this is what attracts users to your brand and product(s).</p>
				<p>Our in-house UI/UX team will create all of the necessary assets (user flowchart, storyboards, wireframes, mockups, graphics, etc.) in the design phase. We'll then use these in combination with your brand guidelines and creative input to help us to create a memorable, user-centric digital experience. </p>
			</div>
			<!-- /.container -->
       	    <div class="text-center"> 
       	    	<a href="https://www.dreamincolor.co/form/" class="call-to-action btn btn-default d-inline-flex align-items-center">LET’S GET STARTED &nbsp; <img src="/dist/img/arrow_right.png" alt=""></a>
       	    </div>
		</div>
		<!-- /.section-banner-bottom -->


		<!-- Our Clients -->
		<div class="most-recent-clients section-our-clients">
			<div class="container">
				<div class="container-inner">											
					<div class="section-header text-center">
						<h2>MOST RECENT CLIENTS</h2>
					</div>
					<div class="row align-items-center logos">
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo5.png" width="170px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo9.png" width="213px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo7.png" width="155px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo12.png" width="186px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo1.png" width="248px" class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo6.png" width="145px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo14.png" width="203px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo16.png" width="78px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo2.png" width="174px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo10.png" width="199px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo4.png" width="226px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo8.png" width="170px" class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo11.png" width="208px" class="img-fluid" alt="">
						</div>

						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo3.png" width="165px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo17.png" width="150px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo18.png" width="150px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo19.png" width="138px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo20.png" width="160px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo21.png" width="200px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo22.png" width="150px"  class="img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo23.png" width="91px"  class="end-logo img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo24.png" width="164px"  class="end-logo img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo25.png" width="146px"  class="end-logo img-fluid" alt="">
						</div>
						<div class="col-6 mt-3  mb-3  mt-md-0 mb-md-0 col-md-3 text-center">
							<img src="/dist/img/logo26.png" width="162px"  class="end-logo img-fluid" alt="">
						</div>
					</div>
					<!-- /.row -->
				</div><!-- /.container-inner -->
			</div>
		</div><!-- /.most-recent-clients -->


		<div class="section-our-recent-projects">
           <div class="section-header">
           	 <div class="container">
           	 	<div class="row">
           	 		<div class="col-md-4 col-lg-3">
           	 			<h2>OUR RECENT PROJECTS</h2>
           	 		</div>
           	 		<div class="col-md-8 col-lg-9">
           	 			<p>NTIR is a data-gathering web application that was designed to be used by a confidential government client to scrape selected websites and pull down there “assets” which included images, PDFs, .DOCX, and a myriad of other document types. After consulting with the technical team, we later decided to expand the app to allow for users to upload documents to the application to increase the user experience. </p>
           	 		</div>
           	 	</div><!-- /.row -->
           	 </div><!-- /.container -->
           </div><!-- /.section-header -->
           <div class="cards container-fluid">
           	    <div class=" project-carousel">	
					<?php
					 
					function getFeed($feed_url) {
						 
						$content = file_get_contents($feed_url);
						$x = new SimpleXmlElement($content);
						 
						foreach($x->channel->item as $entry) {?>
							<div class="project">
								<div class="card h-100">
								  <div class="overlay"></div>
								  <div class="card-body d-flex align-items-center flex-column">
									<div class="text-center">
										<img class="img-fluid" src="/dist/img/group-13.png" alt="Card image cap">
									</div>
									<p class="card-text"><?php $entry->title; ?></p>
									<div class="text-center mt-auto">
										<a href="<?php $entry->link; ?>" class="btn btn-primary">VIEW THIS PROJECT</a>
									</div>
								  </div>
								</div>
							</div>
					    <?php
					    }
					}
					?>
					<?php getFeed("http://www.dreamincolor.co/blog/feed/"); ?>
					
					

           	    </div><!-- /.row -->
           	    <div class="text-center"> 
           	    	<a href="#" class="schedule-call-to-action btn btn-default d-inline-flex align-items-center">SCHEDULE A DEMO &nbsp; <img src="/dist/img/arrow_right.png" alt=""></a>
           	    </div>
           </div><!-- /.cards -->
		</div><!-- /.section-our-recent-projects -->



		<!-- Testimonials  -->
		<div class="section-testimonials landing-page-testimonial-section">
			<div id="testimonials" class="carousel slide " data-ride="carousel">
				<!-- <ol class="carousel-indicators">
					<li data-target="#testimonials" data-slide-to="0" class="active"></li>
					<li data-target="#testimonials" data-slide-to="1"></li>
					<li data-target="#testimonials" data-slide-to="2"></li>
				</ol> -->
				<div class="carousel-inner">
					<div class="carousel-item active" style="background:url(../dist/img/landing_page_testimonial_bg.jpg) no-repeat; background-size: cover; background-position: center top;">
						<!-- <div class="overlay"></div> -->
						<div class="container">
							<div class="testimonial">
								<div class="logo">
									<img src="/dist/img/logo-alqimi.png" alt="" class="img-fluid">
								</div>
								<div class="comment">
									"Working with Chris and Amy was an amazing experience. They used a proven process, prioritized tasks according to impact and business value, and delivered valuable assets to us at each milestone. They also provided us with well rounded solutions and support throughout the time that we worked together."
								</div>
								<!-- /.comment -->
								<div class="small_bar"></div>
								<div class="author">
									JOSH GOLDSBERRY &AMP; AARON HARVEY ALQIMI
								</div>
								<!-- /.author -->
							</div>
						</div>
					</div>
				</div><!-- /.carousel-inner -->
				<!-- <a class="carousel-control-prev" href="#testimonials" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#testimonials" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
				</a> -->
			</div>
		</div>
		<!-- /.section-testimonials -->



        
        <div class="section-our-process2">
        	<div class="container">
        		<h2>OUR PROCESS</h2>
        		<h3>Ideal User Behavior Methodology (IUBM)</h3>
        		<p>How do I know my solution is working well for my users? Gathering data and insights to answer performance driven questions will position you to take the right next step(s).</p>
        		<p>Here's the proven four step methodology that we use during design and development to guide users to success:</p>

        		<div class="steps">	
                	<div class="step">	
                    	<div class="icon-wrap">
                    		<img src="/dist/img/icon_exploration.png" alt="" class="img-fluid">
                    	</div>
                    	<div class="content">
                    		<h5>1. EXPLORE</h5>
                            <p>All effective solutions must stimulate users to explore without any difficulty. This part of the process is critical for your users to build trust in your solution.</p>
                    	</div>
                	</div><!-- /.step -->

                	<div class="step">	
                    	<div class="icon-wrap">
                    		<img src="/dist/img/icon_analyze.png" alt="" class="img-fluid">
                    	</div>
                    	<div class="content">
                    		<h5>2. ANALYZE</h5>
                            <p>When intutive UI patterns are combined with the implemenation of the appropriate techology, this allows all users to analyze and validate the results of any solution.</p>
                    	</div>
                	</div><!-- /.step -->


                	<div class="step">	
                    	<div class="icon-wrap">
                    		<img src="/dist/img/icon_integrate.png" alt="" class="img-fluid">
                    	</div>
                    	<div class="content">
                    		<h5>3. INTEGRATE</h5>
                            <p>The intergration of other solutions (social media channels, api’s, etc.) is a powerful asset! Allowing sharing, linking, and exporting to other tools allows for your solution to become a valuable part of the ever-growing digital ecosystem.</p>
                    	</div>
                	</div><!-- /.step -->


                	<div class="step">	
                    	<div class="icon-wrap">
                    		<img src="/dist/img/icon_take_action.png" alt="" class="img-fluid">
                    	</div>
                    	<div class="content">
                    		<h5>4. TAKE ACTION!</h5>
                            <p>How do I know my solution is working well for my users? Gathering the correct data and insights to answer performance driven questions allows for you to take the right next steps.</p>
                    	</div>
                	</div><!-- /.step -->
        		</div><!-- /.steps -->

        	</div><!-- /.container -->
        </div><!-- /.section-our-process2 -->

        
        <!-- USEFUL UI/UX STATS -->
		<div class="section-useful-ui-ux-stats">
			<div class="container-fluid">
				<h2 class="text-center">USEFUL UI/UX STATS!</h2>
				<div class="stats mt-4">
					<div class="stat">
						<div class="percent">
							<h2>15%</h2>
						</div>
						<div class="content">
							<p> Time.com’s bounce rate dropped 15 percentage points after they adopted continuous scroll.</p>
						</div>
						<a href="#">SOURCE</a>
					</div>
					<div class="stat">
						<div class="percent">
							<h2>88%</h2>
						</div>
						<div class="content">
							<p> 88% of online consumers are less likely to return to a site after a bad experience.</p>
						</div>
						<a href="#">SOURCE</a>
					</div>
					<div class="stat">
						<div class="percent">
							<h2>60%</h2>
						</div>
						<div class="content">
							<p>88% of online consumers are less likely to return to a site after a bad experience.</p>
						</div>
						<a href="#">SOURCE</a>
					</div>
					<div class="stat">
						<div class="percent">
							<h2>88%</h2>
						</div>
						<div class="content">
							<p> 88% of online consumers are less likely to return to a site after a bad experience.</p>
						</div>
						<a href="#">SOURCE</a>
					</div>
					<div class="stat">
						<div class="percent">
							<h2>60%</h2>
						</div>
						<div class="content">
							<p>88% of online consumers are less likely to return to a site after a bad experience.</p>
						</div>
						<a href="#">SOURCE</a>
					</div>
					<div class="stat">
						<div class="percent">
							<h2>88%</h2>
						</div>
						<div class="content">
							<p> 88% of online consumers are less likely to return to a site after a bad experience.</p>
						</div>
						<a href="#">SOURCE</a>
					</div>
					<div class="stat">
						<div class="percent">
							<h2>60%</h2>
						</div>
						<div class="content">
							<p>88% of online consumers are less likely to return to a site after a bad experience.</p>
						</div>
						<a href="#">SOURCE</a>
					</div>

				</div>
			</div>
		</div>



       <!-- Section Schedule and Live Chat -->
       <div class="section-schedule-and-live-chat ">										
           <div class="section-header">
               <div class="container text-center">
               		<h5>How can the IUBM impact your organization?  </h5>
                    <p>Schedule a consultation or live chat to get the answers you are looking for!</p>
               </div>
           </div><!-- /.section-header -->
           <div class="schedule-a-consultation-and-live-chat container">
           	    <div class="row  align-items-center justify-content-center">
           	    	<div class="col-md-5">
           	    		<a href="#">
	           	    		<div class="schedule-a-consultation text-center blue-box">
		           	    		<img src="/dist/img/calendar-check-regular.png" alt="">
		           	    		<h6>SCHEDULE A CONSULTATION</h6>	
	           	    		</div><!-- /.schedule-a-consultation -->
           	    		</a>									
           	    	</div>
           	    	<div class="col-md-2 text-center">
           	    		<img class="mt-3 d-none d-md-block ml-auto mr-auto" src="/dist/img/purple_arrow_left.png" alt="">
           	    		<div class="or mt-4 mb-4 mt-md-2 mb-md-2">OR</div>
           	    		<img class="mb-3 d-none d-md-block ml-auto mr-auto" src="/dist/img/purple_arrow_right.png" alt="">
           	    	</div>
           	    	<div class="col-md-5">
           	    		<a href="#">
	           	    		<div class="live-chat text-center blue-box">
	           	    			<img src="/dist/img/live_chat.jpg" alt="">
	           	    			<h6>LIVE CHAT NOW</h6>
	           	    		</div><!-- /.live-chat -->
           	    		</a>
           	    	</div>
           	    </div><!-- /.row -->
           </div><!-- /.container -->
       </div><!-- /.section-schedule-and-live-chat -->



		<!--Our Design Services  -->
		<div class="section-our-design-services">
			<div class="section-header">
				<div class="container text-center">
					<h2>OUR DESIGN SERVICES</h2>
					<p>NTIR is a data-gathering web application that was designed to be used by a confidential government client to scrape selected websites and pull down there “assets” which included</p>
				</div><!-- /.container -->
			</div><!-- /.section-header -->
			<div class="cards container-fluid">
				<div class="row">
					<div class="col-md-4">
						<div class="card h-100">
						  <div class="card-body  d-flex align-items-center flex-column">
						    <h5 class="card-title">UI/UX</h5>
						    <h6 class="card-subtitle">DESIGN</h6>
						    <p class="card-text">NTIR is a data-gathering web application that was designed to be used by a confidential government client to scrape selected websites and pull down there “assets” which included images, PDFs, .DOCX, and a myriad of other document types. After consulting with the technical team, we later decided to expand the app to allow for users to upload documents to the application to</p>
						    <a href="https://www.dreamincolor.co/form/" class="card-link mt-auto">SCHEDULE A DEMO</a>
						  </div>
						</div>
					</div><!-- /.col-md-4 -->
					<div class="col-md-4">
						<div class="card h-100">
						  <div class="card-body  d-flex align-items-center flex-column">
						    <h5 class="card-title">FRONT-END</h5>
						    <h6 class="card-subtitle">DEVELOPMENT</h6>
						    <p class="card-text">NTIR is a data-gathering web application that was designed to be used by a confidential government client to scrape selected websites and pull down there “assets” which included images, PDFs, .DOCX, and a myriad of other document types. After consulting with the technical team, we later decided to expand the app to allow for users to upload documents to the application to which included images, PDFs, .DOCX, and a myriad of other document types. After consulting with the technical team, we later decided</p>
						    <a href="https://www.dreamincolor.co/form/" class="card-link  mt-auto">SCHEDULE A DEMO</a>
						  </div>
						</div>
					</div><!-- /.col-md-4 -->
					<div class="col-md-4">
						<div class="card h-100">
						  <div class="card-body  d-flex align-items-center flex-column">
						    <h5 class="card-title">BOTH</h5>
						    <h6 class="card-subtitle">DESIGN + DEVELOPMENT</h6>
						    <p class="card-text">NTIR is a data-gathering web application that was designed to be used by a confidential government client to scrape selected websites and pull down there “assets” which included images, PDFs, .DOCX, and a myriad of other document types. After consulting with the technical team, we later decided to expand the app to allow for users to upload documents to the application to </p>
						    <a href="https://www.dreamincolor.co/form/" class="card-link  mt-auto">SCHEDULE A DEMO</a>
						  </div>
						</div>
					</div><!-- /.col-md-4 -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->

			<!-- subscribtion form  -->
			<div class="container">
				<div class="subscribtion-form">
					<form id="mc-form" action="#">
						<input type="email" id="mc-email" placeholder="Enter your email to learn more">
						<label for="mc-email"></label>
						<input type="submit">
					</form>
				</div>
			</div><!-- /.container -->
		</div><!-- /.section-our-design-services -->

        
        <!-- About Us -->
        <div class="section-about-us-why-us">
        	<div class="container">
        		<h2>ABOUT US</h2>
				<p>We believe in using technology for the betterment of humanity. Dream In Color is a 100% Woman, Minority, and Parent-owned business.</p> 
				<p>Amy Oughton and Chris Brooks are married and they manage their business like they manage their family life. They focus on creating leaders, inspiring their team of humanitarians, cultivating new ideas, praising progress/results, respecting all opinions, teaching, learning and enjoying every moment of it!</p>
				<p>May you forever Dream In Color! </p>

        		<h2>WHY US</h2>
				<p>Our CEO is one of the top 3% of design specialists in the world! Our CTO worked as a tech lead to build technology that raised well over $1 Billion for an alumni donation campaign for Georgetown University.</p> 
				<p>We believe that technology is here to make a difference for all - to breakdown walls and shape our futures, to educate, to empower, and protect our resources. </p>
				<p>We want to be a part of a greater tommorrow!</p>
        	</div><!-- /.container -->

       	    <div class="text-center"> 
       	    	<a href="https://www.dreamincolor.co/form/" class="call-to-action btn btn-default d-inline-flex align-items-center">LET’S DO THIS! &nbsp; <img src="/dist/img/arrow_right.png" alt=""></a>
       	    </div>
        </div><!-- /.about-us-section -->
		


		<!-- Site Footer -->
		<footer class="site-footer">
		    <div class="container">
		        <div class="row">
		            <div class="col-md-6 order-md-2  social-icons-area">
		                <ul class="social-icons">
		                    <!-- <li>
		                        <a href="#">
		                            <i class="fab fa-twitter"></i>
		                        </a>
		                    </li> -->
		                    <li>
		                        <a href="https://www.facebook.com/dreamincolor.co/" target="_blank">
		                            <i class="fab fa-facebook-f"></i>
		                        </a>
		                    </li>
		                    <!-- <li>
		                        <a href="#">
		                            <i class="fab fa-instagram"></i>
		                        </a>
		                    </li> -->
		                    <li>
		                        <a href="https://www.linkedin.com/company/dream-in-color-llc/" target="_blank">
		                            <i class="fab fa-linkedin"></i>
		                        </a>
		                    </li>
		                    <!-- <li>
		                        <a href="#">
		                            <i class="fab fa-reddit"></i>
		                        </a>
		                    </li>
		                    <li>
		                        <a href="#">
		                            <i class="fab fa-medium"></i>
		                        </a>
		                    </li> -->
		                </ul>
		            </div>
		            <div class="col-md-6 order-md-1 align-self-center">
		                <p class="copyright">Copyright &copy; Dream In Color, 2018</p>
		            </div>
		        </div>
		    </div>
		</footer>

	</div>
	<!-- /.main-wrapper -->
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- jQuery -->
	<script src="/src/js/jquery.min.js"></script>
	<!-- <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script> -->
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
	    crossorigin="anonymous"></script>-->

	<script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="/dist/js/slick.min.js"></script>
	<script src="/dist/js/bootstrap.min.js"></script>
	<script src="/dist/js/fullpage.min.js"></script>
	<script src="/src/js/jquery.ajaxchimp.js"></script>
	<script src="/src/js/scrolloverflow.min.js"></script>
	<script src="/src/js/custom.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
   -->
	<script type="text/javascript">
		(function () {
			window.SIG_EXT = {};
		})()
	</script>

	<!-- Google Analytics -->
	<script>
		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
		ga('create', 'UA-33871281-1', 'auto');
		ga('send', 'pageview');
	</script>

	<!--Begin Bant.io Code-->
	<script type='text/javascript'>
		(function () {
			var s = document.createElement('script');
			s.type = 'text/javascript';
			s.async = true;
			s.src = '//app.bant.io/5b4f33c638de7?random=' + new Date().getTime();
			var x = document.getElementsByTagName('script')[0];
			x.parentNode.insertBefore(s, x);
		})();
	</script>
	<!--End Bant.io Code-->
	
	<!--PROOF PIXEL-->
	<script src="https://cdn.useproof.com/proof.js?acc=079qek0koRR7LRBNQu5iYiDHYdb2" async></script>
	<!--END PROOF PIXEL-->

</body>

</html>