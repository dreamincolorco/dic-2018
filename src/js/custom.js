$(document).ready(function () {
  // Carousel
  $('.carousel').carousel({
    interval: 5000
  })
});





// Form submission.
$(document).ready(function () {
    // Contact Form
    $(document).on('submit', '#contactForm', function (e) {
      e.preventDefault();
      var datastring = $("#contactForm").serialize();
      console.log('Form  values: ' + datastring);
      $.ajax({
        type: "POST",
        url: "/contact/submit.php",
        data: datastring,
        beforeSend: function () {
          $('.section-contact-form .form-status').addClass('d-none');
          $('.section-contact-form .form-status-info').removeClass('d-none');
        },
        success: function (data) {
            $('.section-contact-form .form-status').addClass('d-none');
            $('.section-contact-form .form-status-success').removeClass('d-none');
        },
        error: function () {
          $('.section-contact-form .form-status').addClass('d-none');
          $('.section-contact-form .form-status-fail').removeClass('d-none');
        }
      });
    });

    // Website Redesign Form
    $(document).on('submit', '#websiteRedesignForm', function (e) {
      e.preventDefault();
      var datastring = $("#websiteRedesignForm").serialize();
      console.log('Form  values: ' + datastring);
      $.ajax({
        type: "POST",
        url: "/form/website-redesign-form-submit.php",
        data: datastring,
        beforeSend: function () {
          $('.section-contact-form .form-status').addClass('d-none');
          $('.section-contact-form .form-status-info').removeClass('d-none');
        },
        success: function (data) {
            $('.section-contact-form .form-status').addClass('d-none');
            $('.section-contact-form .form-status-success').removeClass('d-none');
            window.location.href = "https://calendly.com/dreamincolorco/30-minute-meeting";
        },
        error: function () {
          $('.section-contact-form .form-status').addClass('d-none');
          $('.section-contact-form .form-status-fail').removeClass('d-none');
        }
      });
    });


});






// Menu overlay functionality
  $(document).on('click', '.site-header .menu-button', function (e) {
    e.preventDefault();
    //$('.section-menu-overlay').addClass('show').fadeOut(0).fadeIn(75);
    $('.section-menu-overlay').addClass('show');
    $('html').addClass('disable-scroll');
    $('body').addClass('disable-scroll');
  });
  $(document).on('click', '.section-menu-overlay .menu-button-close', function (e) {
    e.preventDefault();
    //$('.section-menu-overlay').removeClass('show').fadeIn(0).fadeOut(75);
    $('.section-menu-overlay').removeClass('show');
    $('html').removeClass('disable-scroll');
    $('body').removeClass('disable-scroll');
  });


// File includes
$(function () {
  $('[id$="-include"]').each(function (e) {
    $(this).load("/includes\\" + $(this).attr("id").replace("-include", "") + ".html");
  });
  //$.fn.fullpage.reBuild();
});



  /*-------------------------------------------------------------
  # Stats carousel
  --------------------------------------------------------------*/

$(document).ready(function(){
    $('.stats').slick({
        dots: false,
        autoplay: true,
        infinite: true,
        speed: 1500,
        slidesToShow: 3,
        pauseOnHover: true,
       // centerMode: true,
        //variableWidth: true,
        //prevArrow: '<img class="a-left prev slick-prev" src="http://dev.skyway10k.com.php72-35.phx1-1.websitetestlink.com/wp-content/themes/skyway10k/images/SKY-home-leftarrow.png" style="width: 35px; height: 65px;" alt="" />',
       // nextArrow: '<img class="a-right next slick-next" src="http://dev.skyway10k.com.php72-35.phx1-1.websitetestlink.com/wp-content/themes/skyway10k/images/SKY-home-rightarrow.png" style="width: 35px; height: 65px;" alt="" />',
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
             // slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
             // slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
             // slidesToScroll: 1
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
  });
});
  


  /*--------------------------------------------------------------
  # Project carousel
  --------------------------------------------------------------*/
$(document).ready(function(){
  $('.projects').slick({
    dots: false,
    arrows: false, 
    autoplay: true,
    infinite: true,
    speed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    pauseOnHover: true
  });

});
  

  /*--------------------------------------------------------------
  # Cases carousel
  --------------------------------------------------------------*/
$(document).ready(function(){
  $('.js-caseSlider').slick({
    dots: true,
    arrows: false, 
    autoplay: true,
    infinite: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    pauseOnHover: true,
    autoplaySpeed: 7500
  });

});


  /*--------------------------------------------------------------
  # Select placeholder color
  --------------------------------------------------------------*/
  $('select').change(function() {
     $(this).addClass('placeholder');
  });




  /*--------------------------------------------------------------
  # MailChimp Form
  --------------------------------------------------------------*/
  $('#mc-form').ajaxChimp({
      url: 'https://dreamincolor.us3.list-manage.com/subscribe/post?u=f8ff45aca1194e9a5131dc71c&amp;id=2f56b80235'
  });







// Form submission.
// (function($){
//   $(document).ready(function(){
//     $(document).on('submit', '#contact-form', function(e){
//       e.preventDefault();
//       var datastring = $("#contact-form").serialize();
//       $.ajax({
//           type: "POST",
//           url: "/submit.php",
//           data: datastring,
//           beforeSend: function() {
//                $('.section-contact-form .form-status').addClass('hide');
//                $('.section-contact-form .form-status-info').removeClass('hide');
//           },
//           success: function(data) {
//             console.log(data);
//             console.log(data == 1);
//              if(data == 1) {
//                $('.section-contact-form .form-status').addClass('hide');
//                $('.section-contact-form .form-status-success').removeClass('hide');
//              } else {
//                $('.section-contact-form .form-status').addClass('hide');
//                $('.section-contact-form .form-status-fail').removeClass('hide');
//              }
//           },
//           error: function() {
//               $('.section-contact-form .form-status').addClass('hide');
//               $('.section-contact-form .form-status-fail').removeClass('hide');
//           }
//       });
//     });

//   });
// })(jQuery);

//Hide Header on Scroll Down; Show on Scroll Up
// var didScroll;
// var lastScrollTop = 0;
// var delta = 5;
// var navbarHeight = $('header').outerHeight();

// $(window).scroll(function(event){
//     didScroll = true;
// });

// setInterval(function() {
//     if (didScroll) {
//         hasScrolled();
//         didScroll = false;
//     }
// }, 250);

// function hasScrolled() {
//     var st = $(this).scrollTop();
    
//     // Make sure they scroll more than delta
//     if(Math.abs(lastScrollTop - st) <= delta)
//         return;
    
//     // If they scrolled down and are past the navbar, add class .nav-up.
//     // This is necessary so you never see what is "behind" the navbar.
//     if (st > lastScrollTop && st > navbarHeight){
//         // Scroll Down
//         $('header').removeClass('nav-down').addClass('nav-up');
//     } else {
//         // Scroll Up
//         if(st + $(window).height() < $(document).height()) {
//             $('header').removeClass('nav-up').addClass('nav-down');
//             //alert();
//         }
//     }
//     lastScrollTop = st;
// };



highlight();

$(window).on("scroll", function(){
  highlight();
});

function highlight(){
  var scroll = $(window).scrollTop();
  var height = $(window).height();

  $(".highlight").each(function(){
    var pos = $(this).offset().top;
    if (scroll+height >= pos) {
      $(this).addClass("active");
    } 
    //console.log(pos);
    //console.log(scroll);
  });
}  